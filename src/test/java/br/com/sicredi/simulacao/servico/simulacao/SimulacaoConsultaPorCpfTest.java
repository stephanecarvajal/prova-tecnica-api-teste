package br.com.sicredi.simulacao.servico.simulacao;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import java.io.IOException;
import org.junit.Test;
import com.google.gson.Gson;
import br.com.sicredi.simulacao.entity.SimulacaoEntity;
import br.com.sicredi.simulacao.entity.response.SimulacaoCpfNaoEncontradoResponse;

public class SimulacaoConsultaPorCpfTest extends SimulacaoBase {

	private static final String CPF_NAO_REGISTRADO_1 = "40450341020";
	private static final String CPF_NAO_REGISTRADO_2 = "79492020092";
	
	private static final Integer HTTP_STATUS_CPF_ENCONTRADO = 200;
	private static final Integer HTTP_STATUS_CPF_NAO_ENCONTRADO = 404;

	@Test
	public void deveEncontrarCpf_ListaComUmRegistro() throws IOException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get(SIMULACAO_OK1);
		limpaTodasSimulacoes();
		adicionaSimulacao(simulacao);
		
		//teste
		given()
		.when()
			.get(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_CPF_ENCONTRADO)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
		;
	}

	@Test
	public void deveEncontrarCpf_ListaComMaisDeUmRegistro() throws IOException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get(SIMULACAO_OK2);
		limpaTodasSimulacoes();
		adicionaSimulacao(simulacao);
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));

		//teste
		given()
		.when()
			.get(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_CPF_ENCONTRADO)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
		;
	}
	
	@Test
	public void deveNaoEncontrarCpf_ListaVazia() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		String cpfTeste = CPF_NAO_REGISTRADO_1;
		String bodyExpected = new Gson().toJson(new SimulacaoCpfNaoEncontradoResponse(cpfTeste));
		
		//teste
		given()
		.when()
			.get(format(URL_PATH, cpfTeste))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_CPF_NAO_ENCONTRADO)
			.body(is(bodyExpected))
		;
	}
	
	@Test
	public void deveNaoEncontrarCpf_ListaComMaisDeUmRegistro() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK2));
		String cpfTeste = CPF_NAO_REGISTRADO_1;
		String bodyExpected = new Gson().toJson(new SimulacaoCpfNaoEncontradoResponse(cpfTeste));

		//teste
		given()
		.when()
			.get(format(URL_PATH, cpfTeste))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_CPF_NAO_ENCONTRADO)
			.body(is(bodyExpected))
		;
	}

	@Test
	public void deveNaoEncontrarCpf_MensagemComOutroCpf() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK2));
		String cpfTeste = CPF_NAO_REGISTRADO_2;
		String bodyExpected = new Gson().toJson(new SimulacaoCpfNaoEncontradoResponse(cpfTeste));

		//teste
		given()
		.when()
			.get(format(URL_PATH, cpfTeste))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_CPF_NAO_ENCONTRADO)
			.body(is(bodyExpected))
		;
	}
	
	
	
	
}


