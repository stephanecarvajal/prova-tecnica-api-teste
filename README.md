# Testes de API
**Nome:** Gloria Stephane Malo Carvajal

## Desenvolvimento:
Para o desenvolvimento, foi utilizado
- Java 8 - openjdk version "1.8.0_292"
- Maven 3.8.1

Para executar os testes, na raiz do projeto execute a linha de comando:
```
mvn clean test
```

## Erros identificado:

### [POST] /api/v1/simulacoes - Insere uma nova simulação
Cenário: Erro quando CPF já existe
* Esperado: 409 com mensagem "CPF já existente"
* Atual: 400 com mensagem "CPF duplicado"

**Regras do atributo CPF:**
Cenário: Não adicionar **CPF** formatado 999.999.999-99
* Esperado: error 400 
* Atual: sucesso 201

Cenário: Não adicionar **CPF** inválido (fora do algoritimo do governo Brasileiro)
* Esperado: error 400 
* Atual: sucesso 201

Cenário: Não adicionar no campo **CPF** outro tipo de valor
* Esperado: error 400 
* Atual: sucesso 201

Cenário: Não adicionar no campo **CPF** texto
* Esperado: error 400 
* Atual: sucesso 201

Cenário: Não adicionar quando campo **CPF** é vazio
* Esperado: error 400 
* Atual: sucesso 201

**Regras do atributo Nome:**
Cenário: Não adicionar quando campo **nome** é vazio
* Esperado: error 400 
* Atual: sucesso 201

**Regras do atributo Seguro:**
Cenário: Não adicionar quando campo **seguro** é vazio
* Esperado: error 400 
* Atual: erro no servidor 500

**Regras do atributo Valor:**
Cenário: Não adicionar quando campo **valor** é menor que o limite
* Esperado: error 400 
* Atual: sucesso 201

Cenário: Não adicionar quando campo **valor** é igual a zero
* Esperado: error 400 
* Atual: sucesso 201

Cenário: Não adicionar quando campo **valor** é número negativo
* Esperado: error 400 
* Atual: sucesso 201

**Regras do atributo Parcelas:**
Cenário: Não adicionar quando campo **parcelas** é maior que o limite
* Esperado: error 400 
* Atual: sucesso 201


### [PUT] /api/v1/simulacoes/{cpf} - Atualiza uma nova simulação
**Regras do atributo CPF:**
Cenário: Não atualizar **CPF** formatado 999.999.999-99
* Esperado: error 400 
* Atual: sucesso 200

Cenário: Não atualizar **CPF** inválido (fora do algoritimo do governo Brasileiro)
* Esperado: error 400 
* Atual: sucesso 200

Cenário: Não atualizar no campo **CPF** outro tipo de valor
* Esperado: error 400 
* Atual: sucesso 200

Cenário: Não atualizar no campo **CPF** texto
* Esperado: error 400 
* Atual: sucesso 200

Cenário: Não atualizar quando campo **CPF** é vazio
* Esperado: error 400 
* Atual: sucesso 200

**Regras do atributo Nome:**
Cenário: Não atualizar quando campo **nome** é vazio
* Esperado: error 400 
* Atual: sucesso 200

**Regras do atributo Seguro:**
Cenário: Não atualizar quando campo **seguro** é vazio
* Esperado: error 400 
* Atual: erro no servidor 500

**Regras do atributo Valor:**
Cenário: Deve atualizar quando campo **valor** 
* Esperado: atualizar valor 
* Atual: não esta atualizando

Cenário: Não atualizar quando campo **valor** é menor que o limite
* Esperado: error 400 
* Atual: sucesso 200

Cenário: Não atualizar quando campo **valor** é igual a zero
* Esperado: error 400 
* Atual: sucesso 200

Cenário: Não atualizar quando campo **valor** é número negativo
* Esperado: error 400 
* Atual: sucesso 200

**Regras do atributo Parcelas:**
Cenário: Não atualizar quando campo **parcelas** é maior que o limite
* Esperado: error 400 
* Atual: sucesso 200



