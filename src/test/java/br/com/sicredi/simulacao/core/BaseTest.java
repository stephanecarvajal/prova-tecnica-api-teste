package br.com.sicredi.simulacao.core;

import static java.lang.String.format;
import org.junit.BeforeClass;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;

public class BaseTest implements Constants {

	@BeforeClass
	public static void setup() {
		
		//Definicoes
		RestAssured.baseURI = APP_BASE_URL;
		RestAssured.port = APP_PORT;
		RestAssured.basePath = APP_BASE_PATH;
		
		//Requisicao base
		RequestSpecBuilder requestBuilder = new RequestSpecBuilder();
		requestBuilder.setContentType(APP_CONTENT_TYPE);
		RestAssured.requestSpecification = requestBuilder.build();
		
		//Resposta base
		ResponseSpecBuilder responseBuilder = new ResponseSpecBuilder();
		RestAssured.responseSpecification = responseBuilder.build();
	}

	protected static String getBaseAddress() {
		return format("%s:%s%s", APP_BASE_URL, APP_PORT, APP_BASE_PATH);
	}
}
