package br.com.sicredi.simulacao.servico.simulacao;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.hasKey;

import java.io.IOException;

import org.junit.Test;

import com.google.gson.Gson;

import br.com.sicredi.simulacao.entity.SimulacaoEntity;
import io.restassured.http.ContentType;

public class SimulacaoAdicionaPorCpfTest extends SimulacaoBase {

	private static final Integer HTTP_STATUS_SUCESSO_ADICIONAR = 201;
	private static final Integer HTTP_STATUS_ERRO_REGISTRO = 400;
	private static final Integer HTTP_STATUS_ERRO_CPF_REGISTRADO = 409;

	@Test
	public void deveAdicionarSimulacao_ListaVazia() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ADICIONAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAdicionarSimulacao_ListaComRegistro() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK2).clone();
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ADICIONAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAdicionarSimulacao_RegraValorLimiteInferior() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setValor(VALOR_LIMITE_INFERIOR);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ADICIONAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_RegraValorMenorLimiteInferior() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setValor(500F);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO_INFERIOR")))
		;
	}
	
	@Test
	public void deveNaoAdicionarSimulacao_RegraValorIgualZero() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setValor(0F);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO_INFERIOR")))
		;
	}
	
	@Test
	public void deveNaoAdicionarSimulacao_RegraValorNegativo() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setValor(-5000F);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveAdicionarSimulacao_RegraValorLimiteSuperior() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setValor(VALOR_LIMITE_SUPERIOR);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ADICIONAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}
	
	@Test
	public void deveNaoAdicionarSimulacao_RegraValorSuperiorLimite() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setValor(48000F);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO_SUPERIOR")))
		;
	}

	@Test
	public void deveAdicionarSimulacao_RegraParcelaLimiteInferior() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setParcelas(PARCELAS_LIMITE_INFERIOR);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ADICIONAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}
	
	@Test
	public void deveNaoAdicionarSimulacao_RegraParcelaInferiorLimite() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setParcelas(PARCELAS_LIMITE_INFERIOR-1);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_RegraParcelaIgualZero() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setParcelas(0);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_RegraParcelaNumeroNegativo() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setParcelas(-5);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveAdicionarSimulacao_RegraParcelaLimiteSuperior() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setParcelas(PARCELAS_LIMITE_SUPERIOR);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ADICIONAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}
	
	@Test
	public void deveNaoAdicionarSimulacao_RegraParcelaSuperiorLimite() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		simulacao.setParcelas(PARCELAS_LIMITE_SUPERIOR+1);
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO_SUPERIOR")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroEmail_SemArroba() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_EMAIL_INVALIDO_SEM_ARROBA");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.email",is(MSG_ERROS.get("EMAIL_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAdicionarSimulacao_ErroEmail_SemPonto() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_EMAIL_INVALIDO_SEM_PONTO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.email",is(MSG_ERROS.get("EMAIL_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroEmail_ApenasTexto() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_EMAIL_INVALIDO_TEXTO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.email",is(MSG_ERROS.get("EMAIL_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroEmail_Vazio() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_EMAIL_VAZIO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.email",is(MSG_ERROS.get("EMAIL_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroCpf_Invalido() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_CPF_INVALIDO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroCpf_Formatado() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_CPF_FORMATADO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroCpf_MenosNumero() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_CPF_MENOS_NUMERO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}


	@Test
	public void deveNaoAdicionarSimulacao_ErroCpf_Letra() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_CPF_LETRA");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroCpf_Vazio() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_CPF_VAZIO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroNome_Vazio() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_NOME_VAZIO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.nome",is(MSG_ERROS.get("NOME_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroSeguro_Vazio() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_SEGURO_VAZIO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.seguro",is(MSG_ERROS.get("SEGURO_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAdicionarSimulacao_ErroValor_Vazio() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_VALOR_VAZIO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAdicionarSimulacao_ErroParcelas_Vazio() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get("ERRO_PARCELAS_VAZIO");
		limpaTodasSimulacoes();
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAdicionarSimulacao_CpfJaCadastrado() throws IOException, CloneNotSupportedException {
		//contexto
		SimulacaoEntity simulacao = (SimulacaoEntity) SIMULACOES.get(SIMULACAO_OK1).clone();
		limpaTodasSimulacoes();
		adicionaSimulacao(simulacao);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.post(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_CPF_REGISTRADO)
			.body(is(emptyString()))
		;
	}
	
}