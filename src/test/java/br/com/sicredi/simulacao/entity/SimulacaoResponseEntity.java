package br.com.sicredi.simulacao.entity;

public class SimulacaoResponseEntity extends SimulacaoEntity {

	private Integer id;

	public SimulacaoResponseEntity() {
		super();
	}

	public SimulacaoResponseEntity(Integer id, String nome, String cpf, String email, Float valor, Integer parcelas,
			Boolean seguro) {
		super(nome, cpf, email, valor, parcelas, seguro);
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
