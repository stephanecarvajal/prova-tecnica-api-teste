package br.com.sicredi.simulacao.servico.simulacao;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import java.io.IOException;
import java.util.Collections;
import org.junit.Test;
import br.com.sicredi.simulacao.entity.SimulacaoEntity;

public class SimulacaoConsultaTodosTest extends SimulacaoBase {

	private static final Integer HTTP_STATUS_SUCESSO = 200;
	
	@Test
	public void deveListarSimulacoes_ListaVazia() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		
		//teste
		given()
		.when()
			.get(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO)
			.body("", equalTo(Collections.emptyList()))
		;
	}
	
	@Test
	public void deveListarSimulacoes_ListaComUmRegistro() throws IOException {
		//contexto
		SimulacaoEntity simulacao = SIMULACOES.get(SIMULACAO_OK1);
		limpaTodasSimulacoes();
		adicionaSimulacao(simulacao);
		
		//teste
		given()
		.when()
			.get(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO)
			.body("", hasSize(1))
			.body("[0].nome",is(simulacao.getNome()))
			.body("[0].cpf",is(simulacao.getCpf()))
			.body("[0].email",is(simulacao.getEmail()))
			.body("[0].valor",is(simulacao.getValor()))
			.body("[0].parcelas",is(simulacao.getParcelas()))
			.body("[0].seguro",is(simulacao.getSeguro()))
		;
	}

	@Test
	public void deveListarSimulacoes_ListaComMaisDeUmRegistro() throws IOException {
		//contexto
		SimulacaoEntity simulacao1 = SIMULACOES.get(SIMULACAO_OK1);
		SimulacaoEntity simulacao2 = SIMULACOES.get(SIMULACAO_OK2);
		limpaTodasSimulacoes();
		adicionaSimulacao(simulacao1);
		adicionaSimulacao(simulacao2);
		
		//teste
		given()
		.when()
			.get(format(URL_PATH, ""))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO)
			.body("", hasSize(2))
			.body("[0].nome",is(simulacao1.getNome()))
			.body("[0].cpf",is(simulacao1.getCpf()))
			.body("[0].email",is(simulacao1.getEmail()))
			.body("[0].valor",is(simulacao1.getValor()))
			.body("[0].parcelas",is(simulacao1.getParcelas()))
			.body("[0].seguro",is(simulacao1.getSeguro()))
			.body("[1].nome",is(simulacao2.getNome()))
			.body("[1].cpf",is(simulacao2.getCpf()))
			.body("[1].email",is(simulacao2.getEmail()))
			.body("[1].valor",is(simulacao2.getValor()))
			.body("[1].parcelas",is(simulacao2.getParcelas()))
			.body("[1].seguro",is(simulacao2.getSeguro()))
		;
	}
	
	
	
}


