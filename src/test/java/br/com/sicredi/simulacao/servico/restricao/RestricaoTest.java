package br.com.sicredi.simulacao.servico.restricao;

import static org.hamcrest.Matchers.*;
import static java.lang.String.format;
import static io.restassured.RestAssured.given;

import br.com.sicredi.simulacao.core.BaseTest;
import br.com.sicredi.simulacao.entity.response.RestricaoResponseResponse;

import org.junit.Test;

import com.google.gson.Gson;

public class RestricaoTest extends BaseTest {
	
	private static final String URL_PATH = "/restricoes/%s";
	
	private static final Integer HTTP_STATUS_COM_RESTRICAO = 200;
	private static final Integer HTTP_STATUS_SEM_RESTRICAO = 204;
	
	private static final String CPF_COM_RESTRICAO_1 = "97093236014";
	private static final String CPF_COM_RESTRICAO_2 = "60094146012";
	private static final String CPF_SEM_RESTRICAO = "60293641030";

	@Test
	public void deveEncontrarRestricao() {
		//contexto
		RestricaoResponseResponse restricaoResponseResponse = new RestricaoResponseResponse(CPF_COM_RESTRICAO_1);
		
		//teste
		given()
		.when()
			.get(format(URL_PATH, CPF_COM_RESTRICAO_1))
		.then().log().all()
			.statusCode(HTTP_STATUS_COM_RESTRICAO)
			.body("mensagem",is(restricaoResponseResponse.getMensagem()))
		;
	}

	@Test
	public void deveEncontrarRestricao_MudaMensagem() {
		//contexto
		RestricaoResponseResponse restricaoResponseResponse = new RestricaoResponseResponse(CPF_COM_RESTRICAO_2);

		//teste
		given()
		.when()
			.get(format(URL_PATH, CPF_COM_RESTRICAO_2))
		.then().log().all()
			.statusCode(HTTP_STATUS_COM_RESTRICAO)
			.body("mensagem",is(restricaoResponseResponse.getMensagem()))
		;
	}

	@Test
	public void deveNaoEncontrarRestricao() {
		//teste
		given()
		.when()
			.get(format(URL_PATH, CPF_SEM_RESTRICAO))
		.then().log().all()
			.statusCode(HTTP_STATUS_SEM_RESTRICAO)
			.body(is(emptyString()))
		;
	}
}
