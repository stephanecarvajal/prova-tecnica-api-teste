package br.com.sicredi.simulacao.entity.response;

public class SimulacaoCpfNaoEncontradoResponse {

	public static final String MSG_PATTERN = "CPF %s não encontrado";

	private String mensagem = "";

	public SimulacaoCpfNaoEncontradoResponse() {
		super();
	}

	public SimulacaoCpfNaoEncontradoResponse(String cpf) {
		super();
		this.mensagem = String.format(MSG_PATTERN, cpf);
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
