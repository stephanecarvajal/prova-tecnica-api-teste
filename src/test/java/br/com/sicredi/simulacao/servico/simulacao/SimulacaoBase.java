package br.com.sicredi.simulacao.servico.simulacao;

import static java.lang.String.format;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import br.com.sicredi.simulacao.core.BaseTest;
import br.com.sicredi.simulacao.entity.SimulacaoEntity;
import br.com.sicredi.simulacao.entity.SimulacaoResponseEntity;

abstract class SimulacaoBase extends BaseTest {

	protected static final String URL_PATH = "/simulacoes/%s";
	protected static final Map<String, SimulacaoEntity> SIMULACOES = new HashMap<String, SimulacaoEntity>();
	protected static final Map<String, String> MSG_ERROS = new HashMap<String, String>();

	protected static final Float VALOR_LIMITE_INFERIOR = 1000F;
	protected static final Float VALOR_LIMITE_SUPERIOR = 40000F;
	protected static final Integer PARCELAS_LIMITE_INFERIOR = 2;
	protected static final Integer PARCELAS_LIMITE_SUPERIOR = 48;

	protected static final String SIMULACAO_OK1 = "OK1";
	protected static final String SIMULACAO_OK2 = "OK2";

	static {
		SIMULACOES.put("OK1",
				new SimulacaoEntity("Gloria Stephane", "60293641030", "stephane@tester.com", 11000F, 3, false));
		SIMULACOES.put("OK2", new SimulacaoEntity("Maria Jose", "77284407050", "majo@tester.com", 28000F, 6, true));
		SIMULACOES.put("ERRO_EMAIL_INVALIDO_SEM_ARROBA",
				new SimulacaoEntity("Gloria Stephane", "60293641030", "www.tester.com", 11000F, 3, false));
		SIMULACOES.put("ERRO_EMAIL_INVALIDO_SEM_PONTO",
				new SimulacaoEntity("Gloria Stephane", "60293641030", "stephane@tester", 11000F, 3, false));
		SIMULACOES.put("ERRO_EMAIL_INVALIDO_TEXTO",
				new SimulacaoEntity("Gloria Stephane", "60293641030", "stephane", 11000F, 3, false));
		SIMULACOES.put("ERRO_EMAIL_VAZIO", new SimulacaoEntity("Gloria Stephane", "60293641030", "", 11000F, 3, false));
		SIMULACOES.put("ERRO_CPF_INVALIDO",
				new SimulacaoEntity("Gloria Stephane", "60293641040", "stephane@tester.com", 11000F, 3, false));
		SIMULACOES.put("ERRO_CPF_FORMATADO",
				new SimulacaoEntity("Gloria Stephane", "602.936.410-30", "stephane@tester.com", 11000F, 3, false));
		SIMULACOES.put("ERRO_CPF_MENOS_NUMERO",
				new SimulacaoEntity("Gloria Stephane", "60293630", "stephane@tester.com", 11000F, 3, false));
		SIMULACOES.put("ERRO_CPF_LETRA",
				new SimulacaoEntity("Gloria Stephane", "cpf", "stephane@tester.com", 11000F, 3, false));
		SIMULACOES.put("ERRO_CPF_VAZIO",
				new SimulacaoEntity("Gloria Stephane", "", "stephane@tester.com", 11000F, 3, false));
		SIMULACOES.put("ERRO_NOME_VAZIO",
				new SimulacaoEntity("", "60293641030", "stephane@tester.com", 11000F, 3, false));
		SIMULACOES.put("ERRO_SEGURO_VAZIO",
				new SimulacaoEntity("Gloria Stephane", "60293641030", "stephane@tester.com", 11000F, 3, null));
		SIMULACOES.put("ERRO_VALOR_VAZIO",
				new SimulacaoEntity("Gloria Stephane", "60293641030", "stephane@tester.com", null, 3, false));
		SIMULACOES.put("ERRO_PARCELAS_VAZIO",
				new SimulacaoEntity("Gloria Stephane", "60293641030", "stephane@tester.com", 11000F, null, false));

		MSG_ERROS.put("EMAIL_INVALIDO", "E-mail deve ser um e-mail válido");
		MSG_ERROS.put("CPF_INVALIDO", "Cpf deve ser um cpf válido");
		MSG_ERROS.put("NOME_INVALIDO", "Nome deve ser um nome válido");
		MSG_ERROS.put("SEGURO_INVALIDO", "Seguro deve ser um seguro válido");
		MSG_ERROS.put("VALOR_INVALIDO", "Valor não pode ser vazio");
		MSG_ERROS.put("VALOR_INVALIDO_INFERIOR", "Valor deve ser maior ou igual a R$ 1.000");
		MSG_ERROS.put("VALOR_INVALIDO_SUPERIOR", "Valor deve ser menor ou igual a R$ 40.000");
		MSG_ERROS.put("PARCELAS_INVALIDO", "Parcelas não pode ser vazio");
		MSG_ERROS.put("PARCELAS_INVALIDO_INFERIOR", "Parcelas deve ser igual ou maior que 2");
		MSG_ERROS.put("PARCELAS_INVALIDO_SUPERIOR", "Parcelas deve ser igual ou menor que 48");

	}

	static List<SimulacaoResponseEntity> getAllSimulacoes() throws IOException {
		String totalSimulacoes = executaRequest("GET", new URL(format("%s/%s", getBaseAddress(), "simulacoes")));
		Type listaTipo = new TypeToken<ArrayList<SimulacaoResponseEntity>>() {
		}.getType();
		return new Gson().fromJson(totalSimulacoes, listaTipo);

	}

	static void limpaTodasSimulacoes() throws IOException {
		List<SimulacaoResponseEntity> listaSimulacoes = getAllSimulacoes();

		for (SimulacaoResponseEntity simulacao : listaSimulacoes) {
			executaRequest("DELETE", new URL(format("%s/%s/%s", getBaseAddress(), "simulacoes", simulacao.getId())));
		}

	}

	static void adicionaSimulacao(SimulacaoEntity simulacao) throws IOException {
		executaRequest("POST", new URL(format("%s/%s", getBaseAddress(), "simulacoes")), new Gson().toJson(simulacao));
	}

	private static String executaRequest(String metodo, URL url) throws IOException {
		return executaRequest(metodo, url, null);
	}

	private static String executaRequest(String metodo, URL url, String body) throws IOException {
		String retorno = "";
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(metodo);
		con.setReadTimeout(5000);
		con.setRequestProperty("Content-Type", "application/json; utf-8");
		if (body != null) {
			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
			osw.write(body);
			osw.flush();
			osw.close();
			os.close();
		}
		InputStream is = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		retorno = br.lines().collect(Collectors.joining());
		br.close();
		isr.close();
		is.close();
		con.disconnect();
		return retorno;
	}

}
