package br.com.sicredi.simulacao.entity.response;

public class RestricaoResponseResponse {

	public static final String MSG_PATTERN = "O CPF %s tem problema";

	private String mensagem = "";

	public RestricaoResponseResponse() {
		super();
	}

	public RestricaoResponseResponse(String cpf) {
		super();
		this.mensagem = String.format(MSG_PATTERN, cpf);
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
