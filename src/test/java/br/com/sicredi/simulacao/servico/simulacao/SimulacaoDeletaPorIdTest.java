package br.com.sicredi.simulacao.servico.simulacao;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import java.io.IOException;
import org.junit.Test;

public class SimulacaoDeletaPorIdTest extends SimulacaoBase {
	
	private static final Integer HTTP_STATUS_SUCESSO = 200;
	private static final String RESPONSE_BODY_SUCESSO = "OK";

	@Test
	public void deveDeletarSimulacao_IdExiste() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		Integer idDeletar = getAllSimulacoes().get(0).getId();
		
		//teste
		given()
		.when()
			.delete(format(URL_PATH, idDeletar))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO)
			.body(is(RESPONSE_BODY_SUCESSO))
		;
	}

	@Test
	public void deveDeletarSimulacao_IdNaoExiste() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		Integer idDeletar = getAllSimulacoes().get(0).getId()+1;
		
		//teste
		given()
		.when()
			.delete(format(URL_PATH, idDeletar))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO)
			.body(is(RESPONSE_BODY_SUCESSO))
		;
	}
	
	
	
	
}


