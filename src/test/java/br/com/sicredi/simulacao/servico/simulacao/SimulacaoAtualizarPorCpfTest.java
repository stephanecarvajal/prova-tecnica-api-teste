package br.com.sicredi.simulacao.servico.simulacao;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasKey;
import java.io.IOException;
import org.junit.Test;
import com.google.gson.Gson;
import br.com.sicredi.simulacao.entity.SimulacaoResponseEntity;
import br.com.sicredi.simulacao.entity.response.SimulacaoCpfNaoEncontradoResponse;
import io.restassured.http.ContentType;

public class SimulacaoAtualizarPorCpfTest extends SimulacaoBase {

	private static final Integer HTTP_STATUS_SUCESSO_ATUALIZAR = 200;
	private static final Integer HTTP_STATUS_ERRO_REGISTRO = 400;
	private static final Integer HTTP_STATUS_ERRO_CPF_NAO_ENCONTRADO = 404;

	@Test
	public void deveAtualizarSimulacao_Nome() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setNome(SIMULACOES.get(SIMULACAO_OK2).getNome());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAtualizarSimulacao_Email() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setEmail(SIMULACOES.get(SIMULACAO_OK2).getEmail());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}
	
	@Test
	public void deveAtualizarSimulacao_Cpf() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setCpf(SIMULACOES.get(SIMULACAO_OK2).getCpf());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAtualizarSimulacao_Seguro() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setSeguro(SIMULACOES.get(SIMULACAO_OK2).getSeguro());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAtualizarSimulacao_Valor() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setValor(SIMULACOES.get(SIMULACAO_OK2).getValor());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAtualizarSimulacao_Parcelas() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setParcelas(SIMULACOES.get(SIMULACAO_OK2).getParcelas());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveNaoAtualizarSimulacao_CpfNaoEncontrado() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setNome(SIMULACOES.get(SIMULACAO_OK2).getNome());
		String cpfTeste = SIMULACOES.get(SIMULACAO_OK2).getCpf();
		String bodyExpected = new Gson().toJson(new SimulacaoCpfNaoEncontradoResponse(cpfTeste));
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, cpfTeste))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_CPF_NAO_ENCONTRADO)
			.body(is(bodyExpected))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Email_SemArroba() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setEmail(SIMULACOES.get("ERRO_EMAIL_INVALIDO_SEM_ARROBA").getEmail());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.email",is(MSG_ERROS.get("EMAIL_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Email_SemPonto() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setEmail(SIMULACOES.get("ERRO_EMAIL_INVALIDO_SEM_PONTO").getEmail());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.email",is(MSG_ERROS.get("EMAIL_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Email_Texto() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setEmail(SIMULACOES.get("ERRO_EMAIL_INVALIDO_TEXTO").getEmail());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.email",is(MSG_ERROS.get("EMAIL_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Email_Vazio() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setEmail(SIMULACOES.get("ERRO_EMAIL_VAZIO").getEmail());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.email",is(MSG_ERROS.get("EMAIL_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAtualizarSimulacao_Cpf_Invalido() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setCpf(SIMULACOES.get("ERRO_CPF_INVALIDO").getCpf());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Cpf_FormatoInvalido() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setCpf(SIMULACOES.get("ERRO_CPF_FORMATADO").getCpf());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAtualizarSimulacao_Cpf_MenosNumero() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setCpf(SIMULACOES.get("ERRO_CPF_MENOS_NUMERO").getCpf());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Cpf_ComLetra() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setCpf(SIMULACOES.get("ERRO_CPF_LETRA").getCpf());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH,simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAtualizarSimulacao_Cpf_Vazio() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setCpf(SIMULACOES.get("ERRO_CPF_VAZIO").getCpf());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.cpf",is(MSG_ERROS.get("CPF_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Nome_Vazio() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setNome(SIMULACOES.get("ERRO_NOME_VAZIO").getNome());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.nome",is(MSG_ERROS.get("NOME_INVALIDO")))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Seguro_Vazio() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setSeguro(SIMULACOES.get("ERRO_SEGURO_VAZIO").getSeguro());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.seguro",is(MSG_ERROS.get("SEGURO_INVALIDO")))
		;
	}

	@Test
	public void deveNaoAtualizarSimulacao_Valor_Vazio() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setValor(SIMULACOES.get("ERRO_VALOR_VAZIO").getValor());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("ERRO_VALOR_VAZIO")))
		;
	}
	
	@Test
	public void deveNaoAtualizarSimulacao_Parcelas_Vazio() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setParcelas(SIMULACOES.get("ERRO_PARCELAS_VAZIO").getParcelas());
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO")))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraValor_LimiteInferior() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setValor(VALOR_LIMITE_INFERIOR);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraValor_MenorQueLimiteInferior() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setValor(VALOR_LIMITE_INFERIOR-1);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraValor_IgualZero() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setValor(0F);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraValor_Negativo() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setValor(-5000F);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraValor_LimiteSuperior() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setValor(VALOR_LIMITE_SUPERIOR);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraValor_MaiorQueLimiteSuperior() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setValor(VALOR_LIMITE_SUPERIOR+1);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.valor",is(MSG_ERROS.get("VALOR_INVALIDO_SUPERIOR")))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraParcelas_LimiteInferior() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setParcelas(PARCELAS_LIMITE_INFERIOR);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraParcelas_MenorQueLimiteInferior() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setParcelas(PARCELAS_LIMITE_INFERIOR-1);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraParcelas_ComZero() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setParcelas(0);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraParcelas_ComNumeroNegativo() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setParcelas(-1);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO_INFERIOR")))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraParcelas_LimiteSuperior() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setParcelas(PARCELAS_LIMITE_SUPERIOR);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_SUCESSO_ATUALIZAR)
			.body("nome",is(simulacao.getNome()))
			.body("cpf",is(simulacao.getCpf()))
			.body("email",is(simulacao.getEmail()))
			.body("valor",is(simulacao.getValor()))
			.body("parcelas",is(simulacao.getParcelas()))
			.body("seguro",is(simulacao.getSeguro()))
			.body("$",hasKey("id"))
		;
	}

	@Test
	public void deveAtualizarSimulacao_RegraParcelas_MaiorQueLimiteSuperior() throws IOException {
		//contexto
		limpaTodasSimulacoes();
		adicionaSimulacao(SIMULACOES.get(SIMULACAO_OK1));
		SimulacaoResponseEntity simulacao = getAllSimulacoes().get(0);
		simulacao.setParcelas(PARCELAS_LIMITE_SUPERIOR+1);
		
		//teste
		given()
	        .contentType(ContentType.JSON)
	        .body(new Gson().toJson(simulacao))
		.when()
			.put(format(URL_PATH, simulacao.getCpf()))
		.then()
			.log().all()
			.statusCode(HTTP_STATUS_ERRO_REGISTRO)
			.body("erros.parcelas",is(MSG_ERROS.get("PARCELAS_INVALIDO_SUPERIOR")))
		;
	}

}