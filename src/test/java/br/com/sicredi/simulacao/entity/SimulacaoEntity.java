package br.com.sicredi.simulacao.entity;

public class SimulacaoEntity implements Cloneable {
	private String nome;
	private String cpf;
	private String email;
	private Float valor;
	private Integer parcelas;
	private Boolean seguro;

	public SimulacaoEntity() {
		super();
	}

	public SimulacaoEntity(String nome, String cpf, String email, Float valor, Integer parcelas, Boolean seguro) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.valor = valor;
		this.parcelas = parcelas;
		this.seguro = seguro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public Integer getParcelas() {
		return parcelas;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public Boolean getSeguro() {
		return seguro;
	}

	public void setSeguro(Boolean seguro) {
		this.seguro = seguro;
	}

	public SimulacaoEntity clone() throws CloneNotSupportedException {
		return (SimulacaoEntity) super.clone();
	}
}
