package br.com.sicredi.simulacao.core;

import io.restassured.http.ContentType;

public interface Constants {

	public static final String APP_BASE_URL = "http://localhost";
	public static final Integer APP_PORT = 8888;
	public static final String APP_BASE_PATH = "/api/v1";
	
	public static final ContentType APP_CONTENT_TYPE = ContentType.JSON;
	
	public static final Long MAX_TIMEOUT = 1000L;
	

}
